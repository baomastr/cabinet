$(document).ready(function(){

    if ($('.popup__datepicker').length){(function ($) {
        $('.popup__datepicker').dateRangePicker({
            inline:true,
            container: '.popup__datepicker',
            alwaysOpen:true,

            singleMonth: true,
            showShortcuts: false,
            showTopbar: false,

            startOfWeek: 'monday',
            separator : ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            autoClose: false,

            batchMode: 'false',
            singleDate : true,

            startDate: new Date(),
            beforeShowDay: function(t)
            {
                var valid = !(t.getDay() == 0 || t.getDay() == 6);  //disable saturday and sunday
                var _class = '';
                var _tooltip = valid ? '' : 'нерабочий день';
                return [valid,_class,_tooltip];
            },

            customArrowPrevSymbol: '<i class=""></i>',
            customArrowNextSymbol: '<i class=""></i>'

        })
    })($);}


    if ($('.datepicker').length){(function ($) {
        $('.datepicker').dateRangePicker({
            inline:true,
            container: '.datepicker',
            alwaysOpen:true,

            singleMonth: true,
            showShortcuts: false,
            showTopbar: false,

            startOfWeek: 'monday',
            separator : ' ~ ',
            format: 'DD.MM.YYYY HH:mm',
            autoClose: false,

            batchMode: 'false',
            singleDate : true,

            startDate: new Date(),
            beforeShowDay: function(t)
            {
                var valid = !(t.getDay() == 0 || t.getDay() == 6);  //disable saturday and sunday
                var _class = '';
                var _tooltip = valid ? '' : 'нерабочий день';
                return [valid,_class,_tooltip];
            },

            customArrowPrevSymbol: '<i class=""></i>',
            customArrowNextSymbol: '<i class=""></i>'

        })
    })($);}

    if ($('input[name="daterange-from"]').length){(function ($) {
        $('input[name="daterange-from"]').dateRangePicker({
            startOfWeek: 'monday',
            separator : ' ~ ',
            format: 'DD.MM.YYYY',

            time: {
                enabled: false
            },

            singleDate: true,
            singleMonth: true,
            autoClose: true,
            customArrowPrevSymbol: '<i class=""></i>',
            customArrowNextSymbol: '<i class=""></i>'
        })
        $('input[name="daterange-to"]').dateRangePicker({
            startOfWeek: 'monday',
            separator : ' ~ ',
            format: 'DD.MM.YYYY',

            time: {
                enabled: false
            },

            singleDate: true,
            singleMonth: true,
            autoClose: true,
            customArrowPrevSymbol: '<i class=""></i>',
            customArrowNextSymbol: '<i class=""></i>'
        })
    })($);}


    /*(function ($) {
        $(".day-schedule").dayScheduleSelector({
            /!*
             days: [1, 2, 3, 5, 6],
             interval: 15,
             startTime: '09:50',
             endTime: '21:06'
             *!/
        });
        $(".day-schedule").on('selected.artsy.dayScheduleSelector', function (e, selected) {
            console.log(selected);
        });
    })($);*/

    /*(function ($) {
        $(".popup__day-schedule").dayScheduleSelector({
            /!*
             days: [1, 2, 3, 5, 6],
             interval: 15,
             startTime: '09:50',
             endTime: '21:06'
             *!/
        });
        $(".popup__day-schedule").on('selected.artsy.dayScheduleSelector', function (e, selected) {
            console.log(selected);
        });
    })($);*/

    $(function () {
        $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
            $(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });
    });


    $('.choose__slider').bxSlider({
        pager: false
    });


    $('.table__content-cell').change(function() {
        $('.checkbox:checked').parent().css('background-color', '#f05163');
        $('.checkbox:not(:checked)').parent().css('background-color', 'white');
    });


    $(function() {
        $('.rate').barrating({
            theme: 'css-stars'
        });
    });


    $(function () {
        var menu = $('.menu');
        var icon = $('.hamburger');
        icon.click(function() {
            $(this).toggleClass('open');
            menu.toggle( "slow", function() {
                // Animation complete.
            });
        });
    });

    if(document.documentElement.clientWidth < 576) {
        $("input[name='pay']").click(function () {
            $('.pay-row').css('display', ($(this).val() === 'a') ? 'flex':'none');
        });
    };

    $(function () {
        var popup = $('.popup');
        var close = $('.popup__close');
        var open = $('.table__content-cell--hov');
        close.click(function() {
            popup.hide( "slow", function() {
                // Animation complete.
            });
        });
        open.click(function () {
            popup.show("slow", function () {

            });
        });
    });


    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

});